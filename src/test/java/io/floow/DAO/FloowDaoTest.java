package io.floow.DAO;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import org.apache.log4j.BasicConfigurator;
import org.bson.Document;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.*;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FloowDaoTest {

    private MongoCollection<Document> collection;
    private MongoClient client;
    private MongoServer server;

    @BeforeAll
    public void setUp() {
        BasicConfigurator.configure();// getting rid of looger warning by leaving it  with a default configuration

        server = new MongoServer(new MemoryBackend());
        // bind on a random local port
        InetSocketAddress serverAddress = server.bind();

        client = new MongoClient(new ServerAddress(serverAddress));
        collection = client.getDatabase("testdb").getCollection("testcollection");
    }

    @AfterAll
    public void tearDown() {
        client.close();
        server.shutdown();
    }
    @AfterEach
    void clearDb() {
        collection.drop();
    }
    @Test
    public void getMongoClientTest(){
        FloowDao floowDao =new FloowDao(client);
        assertEquals(client,floowDao.getMongoClient());
    }

    @Test
    public void persistDocTest(){
        FloowDao floowDao =new FloowDao(client);
        Document doc =new Document("word","test");
        floowDao.persistDoc(doc,collection);
        assertEquals(doc,collection.find().first());
    }
    @Test
    public void getMaxWordTest(){
        FloowDao floowDao =new FloowDao(client);
        Document doc =new Document("word","test");
        Document doc2 =new Document("word","test");
        Document doc3 =new Document("word","test");
        floowDao.persistDoc(doc,collection);
        floowDao.persistDoc(doc2,collection);
        floowDao.persistDoc(doc3,collection);
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("test", 3);
        assertEquals(map,floowDao.getMaxWord(collection));
    }
    @Test
    public void getMinWordTest(){
        FloowDao floowDao =new FloowDao(client);
        Document doc =new Document("word","test");
        Document doc2 =new Document("word","test");
        Document doc3 =new Document("word","test");
        Document doc4 =new Document("word","min");
        Document doc5 =new Document("word","min");
        floowDao.persistDoc(doc,collection);
        floowDao.persistDoc(doc2,collection);
        floowDao.persistDoc(doc3,collection);
        floowDao.persistDoc(doc4,collection);
        floowDao.persistDoc(doc5,collection);
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("min", 2);
        assertEquals(map,floowDao.getMinWord(collection));
    }
}
