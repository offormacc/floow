package io.floow.model;

import lombok.Data;

@Data
public class MongoConnectionParameter {
    private String host;
    private Integer port;

    public MongoConnectionParameter(String host, Integer port) {
        this.host = host;
        this.port = port;
    }
}
