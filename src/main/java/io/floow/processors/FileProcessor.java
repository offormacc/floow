package io.floow.processors;

import io.floow.model.Word;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.BlockingQueue;

public class FileProcessor implements Runnable {

    private BlockingQueue<String> queue;
    private Path filePath;
    private String startElement;

    public FileProcessor(BlockingQueue<String> q, Path filePath, String startElement) {
        this.queue = q;
        this.filePath = filePath;
        this.startElement = startElement;
    }

    @Override
    public void run() {
        int count = 0;
        QName name = new QName(startElement);

        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = null;

        try {
            File file = filePath.toFile();
            reader = inputFactory.createXMLEventReader(new FileReader(file));
            String fileName;
            while (true) {
                XMLEvent event = reader.nextEvent();
                fileName = file.getParentFile() + File.separator + "dump" + (count++) + ".xml";
                if (event.isStartElement()) {
                    StartElement element = event.asStartElement();
                    if (element.getName().equals(name)) {
                        writeToFile(reader, event, fileName);
                        queue.put(fileName);
                        System.out.println("File " + fileName + " Written and pushed to queue");
                    }
                }
                if (event.isEndDocument())
                    break;
            }
            queue.put(Word.QUEUE_END_VALUE);
            System.out.println("Source file processed successfully");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * <p>This method reads from the large file and writes valid chunks of xml to file
     * </p>
     *
     * @param reader     XMLEventReader used to read file
     * @param startEvent XMLEvent of the database to query
     * @param filename   string of file name to be read
     * @return void
     * @throws XMLStreamException
     * @throws IOException
     */
    private void writeToFile(XMLEventReader reader,
                             XMLEvent startEvent,
                             String filename)
            throws XMLStreamException, IOException {
        StartElement element = startEvent.asStartElement();
        QName name = element.getName();
        int stack = 1;
        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
        XMLEventWriter writer = outputFactory.createXMLEventWriter(new FileWriter(filename));
        writer.add(element);
        while (true) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()
                    && event.asStartElement().getName().equals(name))
                stack++;
            if (event.isEndElement()) {
                EndElement end = event.asEndElement();
                if (end.getName().equals(name)) {
                    stack--;
                    if (stack == 0) {
                        writer.add(event);
                        break;
                    }
                }
            }
            writer.add(event);
        }
        writer.close();
    }
}
