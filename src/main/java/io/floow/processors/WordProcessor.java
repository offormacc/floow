package io.floow.processors;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.floow.DAO.FloowDao;
import io.floow.model.Word;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.bson.Document;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

public class WordProcessor implements Runnable {

    private BlockingQueue<String> queue;
    private String wordTag;
    private FloowDao floowDao;

    public WordProcessor(BlockingQueue<String> queue, String wordTag, FloowDao floowDao) {
        this.queue = queue;
        this.wordTag = wordTag;
        this.floowDao = floowDao;
    }


    @Override
    public void run() {
        String file;
        MongoDatabase clientDB = floowDao.getMongoClient().getDatabase(Word.DB_NAME);
        MongoCollection<Document> collection = clientDB.getCollection(Word.COLLECTION_NAME);
        try {
            while (!(file = queue.take()).equals(Word.QUEUE_END_VALUE)) {
                if (Files.exists(Paths.get(file), new LinkOption[]{LinkOption.NOFOLLOW_LINKS})) {
                    List<String> collect = Files.lines(Paths.get(file))
                            .map(val -> {
                                return StringUtils.substringBetween(val, "<title>", "</title>")
                                        + " " + StringUtils.substringBetween(val, "<abstract>", "</abstract>")
                                        + " " + StringUtils.substringBetween(val, "<" + wordTag + ">", "</" + wordTag + ">");
                            })
                            .filter(value -> !StringUtils.isEmpty(value)).collect(Collectors.toList());
                    sanitizewords(collect, file, collection);
                }
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * <p>This method spilts and sanitizes the words
     * </p>
     *
     * @param words list of words to be santized
     * @return void
     */
    private void sanitizewords(List<String> words, String file, MongoCollection<Document> collection) {
        words.forEach(word -> {
            String[] manyWords = word.split(" ");
            if (manyWords.length > 0) {
                for (int i = 0; i < manyWords.length; i++) {
                    if (!containsNumber(manyWords[i]) && !manyWords[i].equals("null")) { // skip nulls and words with numbers
                        try {
                            String cleanWord = manyWords[i];
                            cleanWord = cleanWord.replaceAll("\\p{Punct}", "");// remove all punctuations
                            persistWord(cleanWord, file, collection);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    /**
     * <p>This method returns true if word does not contain numbers.
     * It will be used to skip words with numbers.
     * </p>
     *
     * @param word the word to be checked for numbers
     * @return boolean. returns true if word contains number
     */
    private boolean containsNumber(String word) {
        return NumberUtils.isCreatable(word);
    }

    private void persistWord(String word, String fileName, MongoCollection<Document> collection) throws IOException {
        synchronized (WordProcessor.class) {
            Document dbObject = new Document("word", word);
            floowDao.persistDoc(dbObject, collection);
        }

        if (Files.exists(Paths.get(fileName), new LinkOption[]{LinkOption.NOFOLLOW_LINKS})) {
            Files.delete(Paths.get(fileName));
            System.out.println("Processed file >>>>>>>>>>>>> " + fileName + " and saved words to db");
        }
    }


}
