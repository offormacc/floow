package io.floow;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import io.floow.model.Word;
import io.floow.processors.FileProcessor;
import io.floow.processors.WordProcessor;
import io.floow.util.FloowCommand;
import io.floow.DAO.FloowDao;
import org.bson.Document;

import java.net.UnknownHostException;
import java.util.concurrent.*;


public class FloowRunner {

    public static void main(String[] args) throws UnknownHostException {
        FloowCommand floowCommand = new FloowCommand();
        BlockingQueue<String> queue = new ArrayBlockingQueue<>(4);
        JCommander commander = JCommander.newBuilder()
                .addObject(floowCommand)
                .build();
        try {
            commander.parse(args);
        } catch (ParameterException e) {
            System.out.println(e.getMessage());
            showHelp(commander);
        }
        if (floowCommand.isHelp()) {
            showHelp(commander);
        }
        MongoClientURI connectionString = new MongoClientURI((String.format("mongodb://%s:%d", floowCommand.getMongo().getHost(), floowCommand.getMongo().getPort())));
        MongoClient mongoClient = new MongoClient(connectionString);
        MongoCollection<Document> collection =mongoClient.getDatabase(Word.DB_NAME).getCollection(Word.COLLECTION_NAME);
        collection.drop();
        FloowDao floowDao= new FloowDao(mongoClient);
        int processors = Runtime.getRuntime().availableProcessors();

        Thread thread = new Thread(new FileProcessor(queue, floowCommand.getSource(), getStartElement(floowCommand)));
        System.out.println(thread.getName());
        thread.start();

        ExecutorService executorService = Executors.newFixedThreadPool(processors);
        executorService.execute(new FileProcessor(queue, floowCommand.getSource(), getStartElement(floowCommand)));
        Future<String> done = executorService.submit(new WordProcessor(queue, getWordTag(floowCommand), new FloowDao(mongoClient)),"done");

        System.out.println("Started Executing::::::::::::::");
        try {
            if ("done".equals(done.get())){
                System.out.println("This is the word with max occurance "+floowDao.getMaxWord(collection).toString());
                System.out.println("This is the word with min occurance "+floowDao.getMinWord(collection).toString());
                System.exit(0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

     private static String getStartElement(FloowCommand floowCommand ){
        if(null==floowCommand.getStartElement()){
            return "doc";
        }
        return floowCommand.getStartElement();
     }
    private static String getWordTag(FloowCommand floowCommand ){
        if(null==floowCommand.getWordTag()){
            return "anchor";
        }
        return floowCommand.getStartElement();
    }
    private static void showHelp(JCommander commander) {
        commander.usage();
        System.exit(0);
    }
}
