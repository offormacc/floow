package io.floow.DAO;

import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import lombok.Getter;
import org.bson.Document;

import java.util.*;

import static com.mongodb.client.model.Sorts.*;

@Getter
public class FloowDao {

    private MongoClient mongoClient;

    public FloowDao(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    /**
     * <p>This method saves documents to the db
     * </p>
     *
     * @param dbObject   document object to be saved to db
     * @param collection Name of the collection to query
     * @return void
     */
    public void persistDoc(Document dbObject, MongoCollection<Document> collection) {
        dbObject.append("id", UUID.randomUUID().toString());
        collection.insertOne(dbObject);
    }

    /**
     * <p>This method querys and prints word with max occurance to screen
     * </p>
     *
     * @param collection Name of the collection to query
     * @return LinkedHashMap<String, Integer>
     */
    public Map<String, Integer> getMaxWord(MongoCollection<Document> collection) {

        AggregateIterable<Document> aggregate = collection.aggregate(
                Arrays.asList(
                        Aggregates.group("$word", Accumulators.sum("count", 1)),
                        Aggregates.sort(orderBy(descending("count"))),
                        Aggregates.limit(1)
                )
        );
        Map<String, Integer> map = new HashMap<String, Integer>();
        MongoCursor<Document> iterator = aggregate.iterator();
        while (iterator.hasNext()) {
            Document next = iterator.next();
            map.put(next.getString("_id"), next.getInteger("count"));
        }
        return map;
    }

    /**
     * <p>This method querys and prints word with min occurance to screen
     * </p>
     *
     * @param collection Name of the collection to query
     * @return LinkedHashMap<String, Integer>
     */
    public Map<String, Integer> getMinWord(MongoCollection<Document> collection) {

        AggregateIterable<Document> aggregate = collection.aggregate(
                Arrays.asList(
                        Aggregates.group("$word", Accumulators.sum("count", 1)),
                        Aggregates.sort(orderBy(ascending("count"))),
                        Aggregates.limit(1)
                )
        );
        Map<String, Integer> map = new HashMap<String, Integer>();
        MongoCursor<Document> iterator = aggregate.iterator();
        while (iterator.hasNext()) {
            Document next = iterator.next();
            map.put(next.getString("_id"), next.getInteger("count"));
        }
        return map;
    }

}
