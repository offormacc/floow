package io.floow.util;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FilePathValidator implements IParameterValidator {
    @Override
    public void validate(String s, String value) throws ParameterException {
        Path path = Paths.get(value);
        if (!validFilePath(path)) {
            throw new ParameterException(value + " does not exist");
        }
        if (!Files.isRegularFile(path, LinkOption.NOFOLLOW_LINKS)) {
            throw new ParameterException(value + " is not a file");
        }
    }

    private boolean validFilePath(Path path) {
        return Files.exists(path, LinkOption.NOFOLLOW_LINKS);
    }
}
