package io.floow.util;

import com.beust.jcommander.IStringConverter;
import io.floow.model.MongoConnectionParameter;

public class MongoCommandFormater implements IStringConverter<MongoConnectionParameter> {
    @Override
    public MongoConnectionParameter convert(String value) {
        String[] s = value.split(":");
        String host = replaceCharacters(s[0]).trim();
        Integer port = Integer.parseInt(replaceCharacters(s[1]).trim());
        return new MongoConnectionParameter(host, port);
    }

    private String replaceCharacters(String value) {
        return value.replaceAll("\\[", "").replaceAll("]", "");
    }
}
