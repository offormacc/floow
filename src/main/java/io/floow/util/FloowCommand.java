package io.floow.util;

import com.beust.jcommander.Parameter;
import io.floow.model.MongoConnectionParameter;
import lombok.Data;

import java.nio.file.Path;

@Data
public class FloowCommand {
    @Parameter(names = {"-help", "-h"}, help = true, description = "This command displays the help information")
    private boolean help;

    @Parameter(names = {"-source","-s"}, arity = 1, validateWith = FilePathValidator.class,
            converter = FilePathConverter.class, required = true,
            description = "This command specifies the source file to be processed by the application")
    private Path source;

    @Parameter(names = {"-mongo","-m"}, converter = MongoCommandFormater.class, required = true,
            description = "This command specifies the mongodb host and port" +
            "in the format -m [hostname]:[port] or -mongo [hostname]:[port]")
    private MongoConnectionParameter mongo;

    @Parameter(names = {"-startElement", "-sE"}, description = "This command specifies the start element of the xml to be read host and port")
    private String startElement;

    @Parameter(names = {"-wordTag", "-wT"}, description = "This command specifies the word element of the xml to be written to mongodb")
    private String wordTag;

}
