# README #

### What is this repository for? ###

* Quick summary

    An application to analyze a large file and find the highest and least occuring words
* Version

        0.1

### How do I get set up? ###

* Summary of set up

     Clone this repository to your machine.
     Navigate to the location where you cloned the repository.
     Run the following command you should have maven installed
        
    ```mvn clean install```
   
   This will generate a jar file in [your cloned repository location]/target folder.
   Run the resulting jar file with the following command
   
   ```java –Xmx8192m -jar challenge.jar –source dump.xml –mongo [hostname]:[port] ```
   or
    ```java –Xmx8192m -jar challenge.jar –s dump.xml –m [hostname]:[port] ```
   
   where challenge.jar is the name of the jar file, dump.xml is the large file to be processed, 
   hostname is the hostname for the server whith a mongoDB instance and port it the port on which the mongoDB instance is running.
   

* Dependencies

    ```mongo-java-driver version 3.8.2```
    
    ```jcommander version 1.78```
    
    ```lombok version 1.18.10```
    
    ```apache.commons version 3.9```
    
* Assumptions

    There are a few assumptions made in this program:
    
     1. I assumed that alphanumeric word should be ignored.
     2. I have only searched to word between title, anchor and abstract tags.
     3. I did not consider word in the urls between link tags as words.
     4. I have only returned the first words in the list for when there are multiple words with the same frequency
            
* Architecture and Design

    My approach in designing the distributed system modeled in this program is that I will have one server dedicated 
    to splitting the large file into smaller chunks. In this simulation, files have been split in such a way that they 
    remain valid xml files to avoid chopping off words mid way. Since 'half words' will defeat the requirement for the system.
    The file splitting server was modeled as a thread for this simulation.
    The next stage is to have the file splitting server send the small chunks of files to a queueing system. For the 
    purpose of this challenge I have implemented the the queueing system using the java class ArrayBlockingQueue.
    The third and final stage is to have a number of servers in the distributed system, read files from the queue. 
    This I did by spinning up threads with the rest of the available processors on my local machine. These threads 
    will pick files in the queue, extract, sanitize and persist words to the mongoDB. Once we have our words
    in the database it is easier for us to run any number of queries and generate a huge number of analytics.
    The Design Pattern applied is the producer consumer design pattern. The FileProcessor class produces files 
    and submits these files to a queue. The consumer class is the Wordprocessor class that takes reads files from the queue


Please note that due to time limitations I have not written a comprehensive unit tests
